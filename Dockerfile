FROM golang:rc-alpine as build

COPY . /src/canary/

WORKDIR /src/canary/

ENV CGO_ENABLED 0
ENV GO111MODULE on

RUN go get . && go build -o /canary

FROM scratch
LABEL maintainer="mark@philoserf.com"
COPY --from=build canary /
CMD ["/canary", "-h"]
